package com.example.modul4;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.squareup.picasso.Picasso;

public class MenuUtama extends AppCompatActivity {

    private static final String TAG = "MainMenu";
    RecyclerView recyclerView;
    private FirebaseAuth mAuth;
    private FirestoreRecyclerAdapter<Items, ItemsHolder> adapter;
    private Button button_load;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_utama);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mAuth = FirebaseAuth.getInstance();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setImageResource(R.drawable.ic_add);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent tambah = new Intent(MenuUtama.this,InputMenu.class);
                startActivity(tambah);
            }
        });

        recyclerView = findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        FirebaseFirestore rootRef = FirebaseFirestore.getInstance();
        Query query = rootRef.collection("items");

        FirestoreRecyclerOptions<Items> options = new FirestoreRecyclerOptions.Builder<Items>().setQuery(query, Items.class).build();

        adapter = new FirestoreRecyclerAdapter<Items, ItemsHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull ItemsHolder holder, int position, @NonNull Items model) {
                holder.setNama(model.getNama());
                holder.setHarga(model.getHarga());
                holder.setImage(model.getNama(), model.getHarga());
            }

            @NonNull
            @Override
            public ItemsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card, viewGroup, false);
                return new ItemsHolder(view);
            }
        };

        //BUTTON
        button_load = findViewById(R.id.button3);
        button_load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.setAdapter(adapter);
            }
        });
    }

    public class ItemsHolder extends RecyclerView.ViewHolder {
        private View view;

        ItemsHolder(View itemView) {
            super(itemView);
            view = itemView;
        }

        void setNama(String nama) {
            TextView textView = view.findViewById(R.id.textView9);
            textView.setText(nama);
        }

        void setHarga(String harga) {
            TextView textView = view.findViewById(R.id.textView10);
            textView.setText(harga);
        }

        void setImage(String nama, String harga) {
            ImageView imageView = view.findViewById(R.id.imageView4);
            Picasso.get().load("https://firebasestorage.googleapis.com/v0/b/modul4-96c4b.appspot.com/o/images%2F"+nama+"-"+harga+"??alt=media&token=3f98e979-0781-4bc2-8b56-6cdabb9d3b6b").into(imageView);


        }
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        if(adapter != null){
            adapter.stopListening();
        }
    }

    private void logout() {
        mAuth.signOut();
        startActivity(new Intent(MenuUtama.this, Login.class));
        finish();
    }

   /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                logout();
                break;
        }
        return super.onOptionsItemSelected(item);
    }*/



}

