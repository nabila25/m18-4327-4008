package com.example.modul4;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;

public class Registrasi extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private Button button_daftar;
    private EditText editText_nama, editText_email, ediText_password;
    private static final String TAG = " registrasi";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi);

        mAuth = FirebaseAuth.getInstance();
        button_daftar = findViewById(R.id.button2);
        editText_nama = findViewById(R.id.editText3);
        editText_email = findViewById(R.id.editText4);
        ediText_password = findViewById(R.id.editText5);

        button_daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email, password;
                email = editText_email.getText().toString();
                password = ediText_password.getText().toString();

                mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Intent mainmenu = new Intent(Registrasi.this, MenuUtama.class);
                            startActivity(mainmenu);
                            finish();
                        } else {
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                        }
                    }
                });
            }
        });
    }


    public void masuk (View view){
        Intent masuk = new Intent(Registrasi.this, Login.class);
        startActivity(masuk);
    }

}

