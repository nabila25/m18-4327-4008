package com.example.modul4;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class InputMenu extends AppCompatActivity {

    private ImageView mAddMessageView;
    private Button tambahFoto, tambahMenu;
    private static final int REQUEST_IMAGE = 2;
    private EditText editText_namamenu,editText_hargamenu,editText_deskripsimenu;
    private String mPhotoUrl;
    private DatabaseReference mFirebaseDatabaseReference;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private static final String TAG = "InputMenu";

    private Uri uri;
    private Bitmap bitmap;

    //firebase
    FirebaseStorage storage;
    StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_menu);

        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();

        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        mAddMessageView = findViewById(R.id.imageView3);
        tambahFoto = findViewById(R.id.button4);
        tambahMenu = findViewById(R.id.button5);

        //firebasestorage
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        tambahFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_IMAGE);
            }
        });

        tambahMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText_namamenu = findViewById(R.id.editText6);
                editText_hargamenu = findViewById(R.id.editText7);
                editText_deskripsimenu = findViewById(R.id.editText8);

                final String nama = editText_namamenu.getText().toString();
                final String harga = editText_hargamenu.getText().toString();
                String deskripsi = editText_deskripsimenu.getText().toString();

                Map<String, Object> item = new HashMap<>();
                item.put("Nama", nama);
                item.put("Harga", harga);
                item.put("Deskripsi", deskripsi);

                db.collection("items")
                        .add(item)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Log.d(TAG, "DocumentSnapshot added with ID: "+documentReference.getId());

                                StorageReference ref = storageReference.child("images/"+nama+"-"+harga);
                                ref.putFile(uri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                                        Intent bebas = new Intent(InputMenu.this, MenuUtama.class);
                                        startActivity(bebas);
                                    }
                                });
                            }
                        });

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_IMAGE){
            if(resultCode == RESULT_OK){
                if(data != null ){
                    uri = data.getData();
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                        mAddMessageView.setImageBitmap(bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        }
    }


}

