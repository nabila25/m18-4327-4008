package com.example.modul4;

public class Items {

    private String Nama, Harga, Deskripsi;

    public Items() {
    }

    public Items(String nama, String harga, String deskripsi) {
        Nama = nama;
        Harga = harga;
        Deskripsi = deskripsi;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String nama) {
        Nama = nama;
    }

    public String getHarga() {
        return Harga;
    }

    public void setHarga(String harga) {
        Harga = harga;
    }

    public String getDeskripsi() {
        return Deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        Deskripsi = deskripsi;
    }
}
