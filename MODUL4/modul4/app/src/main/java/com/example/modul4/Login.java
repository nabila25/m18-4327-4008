package com.example.modul4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android .support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;

public class Login extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private EditText editText_email, editText_password;
    private Button button_masuk;

    private static final String TAG = "Login";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();

        editText_email= findViewById(R.id.editText);
        editText_password= findViewById(R.id.editText2);
        button_masuk=findViewById(R.id.button);

        button_masuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email,password;
                email = editText_email.getText().toString();
                password = editText_password.getText().toString();

                mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            FirebaseUser user = mAuth.getCurrentUser();
                            Intent mainmenu = new Intent(Login.this, MenuUtama.class);
                            startActivity(mainmenu);
                            finish();
                        }else{
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                        }
                    }
                });
            }
        });

    }
    public void Register(View view) {
        Intent Register= new Intent(Login.this,Registrasi.class);
        startActivity(Register);
    }
}



