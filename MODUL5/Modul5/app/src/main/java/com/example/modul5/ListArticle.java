package com.example.modul5;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class ListArticle extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    ConstraintLayout constraintLayout;
    RecyclerView recyclerView;

    //DB
    private ArticleDbHelper db;
    private List<Article> articleList = new ArrayList<Article>();
    private ArticleAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_article);
        getSupportActionBar().setTitle("List Article");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Initiate
        constraintLayout = findViewById(R.id.Constraintlistarticle);
        recyclerView = findViewById(R.id.recyclerView);
        sharedPreferences = getSharedPreferences("setting", Context.MODE_PRIVATE);

        //Check if Nightmode is true
        boolean nightmodestatus = sharedPreferences.getBoolean("nightmode", false);
        if(nightmodestatus == true){
            intoDarkMode();
        }else{
            intoLightMode();
        }

        db = new ArticleDbHelper(this);
        articleList.addAll(db.allArticles());

        adapter = new ArticleAdapter(this, articleList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                Intent intent = new Intent(ListArticle.this, MainActivity.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void intoDarkMode(){
        constraintLayout.setBackgroundColor(Color.BLACK);
    }

    public void intoLightMode(){
        constraintLayout.setBackgroundColor(Color.WHITE);
    }

}


