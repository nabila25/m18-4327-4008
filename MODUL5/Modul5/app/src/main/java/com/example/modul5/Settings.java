package com.example.modul5;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;


public class Settings extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    Switch nightMode, textSize;
    ConstraintLayout constraintLayout;
    SharedPreferences.Editor editor;
    Button save;
    TextView judul;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getSupportActionBar().setTitle("Setting");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        nightMode = findViewById(R.id.switch1);
        textSize = findViewById(R.id.switch2);
        constraintLayout = findViewById(R.id.Constraintsetting);
        save = findViewById(R.id.button);
        sharedPreferences = getSharedPreferences("setting", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        judul = findViewById(R.id.textView);

        //Check if Nightmode is true
        boolean nightmodestatus = sharedPreferences.getBoolean("nightmode", false);
        if(nightmodestatus == true){
            intoDarkMode();
            nightMode.setChecked(true);
        }else{
            intoLightMode();
            nightMode.setChecked(false);
        }

        //Check if Textsize is true
        boolean textsizestatus = sharedPreferences.getBoolean("textsize", false);
        if(textsizestatus == true){
            textSize.setChecked(true);
        }else{
            textSize.setChecked(false);
        }

        //Switch Nightmode
        nightMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked == true){
                    setNightMode(true);
                    intoDarkMode();
                }else{
                    setNightMode(false);
                    intoLightMode();
                }
            }
        });

        //Switch Textsize
        textSize.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked == true){
                    setTextSize(true);
                }else{
                    setTextSize(false);
                }
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.commit();
                Intent intent = new Intent(Settings.this, Menu.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                Intent intent = new Intent(Settings.this, Menu.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void setNightMode(Boolean state){
        editor.putBoolean("nightmode", state);
    }

    public void setTextSize(Boolean state){
        editor.putBoolean("textsize", state);
    }

    public void intoDarkMode(){
        constraintLayout.setBackgroundColor(Color.BLACK);
        nightMode.setTextColor(Color.WHITE);
        textSize.setTextColor(Color.WHITE);
        judul.setTextColor(Color.WHITE);

    }

    public void intoLightMode(){
        constraintLayout.setBackgroundColor(Color.WHITE);
        nightMode.setTextColor(Color.BLACK);
        textSize.setTextColor(Color.BLACK);
        judul.setTextColor(Color.BLACK);
    }

}

