package com.example.modul5;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

public class ViewArticle extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    ConstraintLayout constraintLayout;
    TextView judulnya, authordantanggal, artikelnya;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_article);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Initiate
        constraintLayout = findViewById(R.id.Constraintviewarticle);
        judulnya = findViewById(R.id.textView8);
        authordantanggal = findViewById(R.id.textView12);
        artikelnya = findViewById(R.id.textView13);
        sharedPreferences = getSharedPreferences("setting", Context.MODE_PRIVATE);

        //Check if Nightmode is true
        boolean nightmodestatus = sharedPreferences.getBoolean("nightmode", false);
        if(nightmodestatus == true){
            intoDarkMode();
        }else{
            intoLightMode();
        }

        //Check if Textsize is true
        boolean textsizestatus = sharedPreferences.getBoolean("textsize", false);
        if(textsizestatus == true){
            artikelnya.setTextSize(26);
        }else{
            artikelnya.setTextSize(14);
        }

        //FetchData
        Intent data = getIntent();
        judulnya.setText(data.getStringExtra("title"));
        authordantanggal.setText(data.getStringExtra("author")+", "+data.getStringExtra("created_at"));
        artikelnya.setText(data.getStringExtra("description"));
        getSupportActionBar().setTitle(data.getStringExtra("title"));
        artikelnya.setMovementMethod(new ScrollingMovementMethod());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                Intent intent = new Intent(ViewArticle.this, ListArticle.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void intoDarkMode(){
        constraintLayout.setBackgroundColor(Color.BLACK);
        judulnya.setTextColor(Color.WHITE);
        authordantanggal.setTextColor(Color.WHITE);
        artikelnya.setTextColor(Color.WHITE);
    }

    public void intoLightMode(){
        constraintLayout.setBackgroundColor(Color.WHITE);
        judulnya.setTextColor(Color.BLACK);
        authordantanggal.setTextColor(Color.BLACK);
        artikelnya.setTextColor(Color.BLACK);
    }
}

