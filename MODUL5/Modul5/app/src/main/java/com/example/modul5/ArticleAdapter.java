package com.example.modul5;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ViewHolder> {

private Context mContext;
private List<Article> articleList;

public ArticleAdapter(Context context, List<Article> articlesList){
        this.mContext = context;
        this.articleList = articlesList;
        }

@NonNull
@Override
public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card, parent, false);
        return new ViewHolder(itemView);
        }

@Override
public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
final Article article = articleList.get(position);
        Log.d("mencoba",article.getAuthor());
        holder.penulis.setText(article.getAuthor());
        holder.judul.setText(article.getTitle());
        holder.tulisan.setText(article.getDescription());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {
        Intent intent = new Intent(mContext, ViewArticle.class);
        intent.putExtra("title",article.getTitle());
        intent.putExtra("author",article.getAuthor());
        intent.putExtra("description",article.getDescription());
        intent.putExtra("created_at",article.getDate());
        mContext.startActivity(intent);
        }
        });
        }

@Override
public int getItemCount() {
        return articleList.size();
        }

public class ViewHolder extends RecyclerView.ViewHolder {

    TextView penulis,judul,tulisan;

    public ViewHolder(@NonNull View itemView) {
        super(itemView);
        penulis = itemView.findViewById(R.id.textView9);
        judul = itemView.findViewById(R.id.textView10);
        tulisan = itemView.findViewById(R.id.textView11);
    }
}

    public void send(String author, String title, String description, String created_at){

    }
}

