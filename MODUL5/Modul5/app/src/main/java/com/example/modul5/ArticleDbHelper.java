package com.example.modul5;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;


public class ArticleDbHelper   extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "artikel.db";
    private static final String TABLE_NAME = "artikel";
    private static final String KEY_ID = "id";
    private static final String KEY_AUTHOR = "author";
    private static final String KEY_TITLE = "title";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_CREATED_AT = "created_at";
    private static final String[] COLUMNS = {KEY_ID, KEY_AUTHOR, KEY_TITLE, KEY_DESCRIPTION, KEY_CREATED_AT};

    private SQLiteDatabase mWritableDB;
    private SQLiteDatabase mReadableDB;

    public ArticleDbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATION_TABLE = "CREATE TABLE artikel (id INTEGER PRIMARY KEY AUTOINCREMENT, author TEXT, title TEXT, description TEXT, created_at TEXT);";
        db.execSQL(CREATION_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public List<Article> allArticles(){
        List<Article> articles = new ArrayList<>();
        String selectQuery = "SELECT * FROM artikel";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                Article article = new Article();
                article.setId(cursor.getInt(0));
                article.setAuthor(cursor.getString(1));
                article.setTitle(cursor.getString(2));
                article.setDescription(cursor.getString(3));
                article.setDate(cursor.getString(4));
                articles.add(article);
            } while (cursor.moveToNext());
        }
        db.close();
        return articles;
    }

    public long insert(String author, String title, String description, String created_at) {
        long newId = 0;
        ContentValues values = new ContentValues();
        values.put("author", author);
        values.put("title", title);
        values.put("description", description);
        values.put("created_at", created_at);
        try {
            if (mWritableDB == null) {mWritableDB = getWritableDatabase();}
            newId = mWritableDB.insert(TABLE_NAME, null, values);
        } catch (Exception e) {
//            Log.d(TAG, "INSERT EXCEPTION! " + e.getMessage());
        }
        return newId;
    }
}
