package com.example.modul5;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CreateArticle extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    ConstraintLayout constraintLayout;
    EditText edit_author, edit_title, edit_article;
    Button create;
    TextView textView;
    private ArticleDbHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_article);
        getSupportActionBar().setTitle("Create Article");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        constraintLayout = findViewById(R.id.Constraintcreatearticle);
        textView = findViewById(R.id.textView2);
        edit_author = findViewById(R.id.editText);
        edit_title = findViewById(R.id.editText2);
        edit_article = findViewById(R.id.editText3);
        create = findViewById(R.id.button2);
        sharedPreferences = getSharedPreferences("setting", Context.MODE_PRIVATE);

        boolean nightmodestatus = sharedPreferences.getBoolean("nightmode", false);
        if(nightmodestatus == true){
            intoDarkMode();
        }else{
            intoLightMode();
        }

        //db
        db = new ArticleDbHelper(this);

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String author = edit_author.getText().toString();
                String title = edit_title.getText().toString();
                String article = edit_article.getText().toString();

                db.insert(author,title,article,getDateTime());

                Intent intent = new Intent(CreateArticle.this,Menu.class);
                startActivity(intent);
                finish();
            }
        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                Intent intent = new Intent(CreateArticle.this, Menu.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void intoDarkMode(){
        constraintLayout.setBackgroundColor(Color.BLACK);
        textView.setBackgroundColor(Color.WHITE);
        edit_author.setBackgroundColor(Color.WHITE);
        edit_title.setBackgroundColor(Color.WHITE);
        edit_article.setBackgroundColor(Color.WHITE);
    }

    public void intoLightMode(){
        constraintLayout.setBackgroundColor(Color.WHITE);


    }

    private String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("EEE d MMM ''yy");
        Date date = new Date();
        return dateFormat.format(date);
    }

}


