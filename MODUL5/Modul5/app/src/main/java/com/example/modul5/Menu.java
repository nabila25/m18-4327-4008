package com.example.modul5;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Menu extends AppCompatActivity {

    ConstraintLayout constraintLayout;
    SharedPreferences sharedPreferences;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        getSupportActionBar().setTitle("Article Apps");

        constraintLayout = findViewById(R.id.Constraintmenu);
        textView = findViewById(R.id.textView3);
        sharedPreferences = getSharedPreferences("setting", Context.MODE_PRIVATE);

        boolean nightmodestatus = sharedPreferences.getBoolean("nightmode", false);
        if(nightmodestatus == true){
            intoDarkMode();
        }else{
            intoLightMode();
        }

    }
    public void setting(View view) {

        Intent intent = new Intent(Menu.this,Settings.class);
        startActivity(intent);
    }

    public void createarticle(View view) {
        Intent intent = new Intent(Menu.this,CreateArticle.class);
        startActivity(intent);
    }

    public void listarticle(View view) {
        Intent intent = new Intent(Menu.this,ListArticle.class);
        startActivity(intent);
    }

    public void intoDarkMode(){
        constraintLayout.setBackgroundColor(Color.BLACK);
        textView.setTextColor(Color.WHITE);
        textView.setText("Good Night");
    }

    public void intoLightMode(){
        constraintLayout.setBackgroundColor(Color.WHITE);
        textView.setTextColor(Color.BLACK);
        textView.setText("Good Morning");
    }
}



