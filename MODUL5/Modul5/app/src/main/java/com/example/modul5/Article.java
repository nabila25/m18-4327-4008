package com.example.modul5;

public class Article {
    int id;
    String Author,Title,Description,Date;

    public Article() {
    }

    public Article(int id, String author, String title, String description, String date) {
        this.id = id;
        Author = author;
        Title = title;
        Description = description;
        Date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

}
