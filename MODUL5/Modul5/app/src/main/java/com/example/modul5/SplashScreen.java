package com.example.modul5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public class SplashScreen extends AppCompatActivity {

    ProgressBar bar;
    SharedPreferences sharedPreferences;
    ConstraintLayout constraintLayout;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        getSupportActionBar().hide();

        constraintLayout = findViewById(R.id.ConstraintSplashscreen);
        textView = findViewById(R.id.textView5);
        sharedPreferences = getSharedPreferences("setting", Context.MODE_PRIVATE);

        boolean nightmodestatus = sharedPreferences.getBoolean("nightmode", false);
        if (nightmodestatus == true) {
            intoDarkMode();
        } else {
            intoLightMode();
        }
        final Handler handler = new Handler();
        bar = findViewById(R.id.progressBar);

        new Thread(new Runnable() {
            @Override
            public void run() {
                doWork();
                startApp();
                finish();
            }
        }).start();

    }

    private void doWork() {
        for (int progress = 0; progress < 100; progress++) {
            try {
                Thread.sleep(50);
                bar.setProgress(progress);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void startApp() {
        Intent intent = new Intent(SplashScreen.this, Menu.class);
        startActivity(intent);
    }

    public void intoDarkMode() {
        constraintLayout.setBackgroundColor(Color.BLACK);
        textView.setTextColor(Color.WHITE);
    }

    public void intoLightMode() {
        constraintLayout.setBackgroundColor(Color.WHITE);

    }
}
