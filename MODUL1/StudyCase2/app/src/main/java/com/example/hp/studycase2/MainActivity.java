package com.example.hp.studycase2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText editText;
    EditText editText2;
    Button button;
    TextView textView;
    TextView textView4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = (EditText) findViewById(R.id.editText);
        editText2 = (EditText) findViewById(R.id.editText2);
        button = (Button) findViewById(R.id.button);
        textView = (TextView) findViewById(R.id.textView);
        textView4 = (TextView) findViewById(R.id.textView4);

        button.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                String inputan = editText.getText().toString();
                String inputan2 = editText2.getText().toString();

                Double editText = Double.parseDouble(inputan);
                Double editText2 = Double.parseDouble(inputan2);

                double hasilnya = luas(editText, editText2);

                String output = String.valueOf(hasilnya);
                textView4.setText(output.toString());


        }
    });
}
public double luas (double editText, double editText2){
    return (editText*editText2);
}
}
